font
========

~~~
font: [ [ <'font-style'> || <'font-variant'> || <'font-weight'> ]? <'font-size'> [ / <'line-height'> ]? <'font-family'> ] | caption | icon | menu | message-box | small-caption | status-bar
~~~ 

Valores

`<'font-style'>` <br>
Veja a propriedade [font-style](/front-end/css/properties/typography/font-style).

`<'font-variant'>` <br>
Veja a propriedade [font-variant](/front-end/css/properties/typography/font-variant).

`<'font-weight'>` <br>
Veja a propriedade [font-weight](/front-end/css/properties/typography/font-weight).

`<'font-size'>` <br>
Veja a propriedade [font-size](/front-end/css/properties/typography/font-size).

`<'line-height'>` <br>
Veja a propriedade [line-height](/front-end/css/properties/typography/line-height).

`<'font-family'>` <br>
Veja a propriedade [font-family](/front-end/css/properties/typography/font-family).


Em vez de especificar as propriedades individuais, uma palavra-chave pode ser usada para representar uma fonte de sistema específico:

`caption` - A fonte usada para controles legendadas (por exemplo, botões, menus suspensos, etc.). <br>
`icon` -  O tipo de letra usado para rotular ícones. <br>
`menu` - A fonte usada em menus (por exemplo, menus suspensos e listas de menu). <br>
`mensagem-box` - letra utilizada nas caixas de diálogo. <br>
`small-caption` - A fonte usada para a rotulagem de pequenos controles. <br>
`status-bar` - A fonte usada em barras de status janela.