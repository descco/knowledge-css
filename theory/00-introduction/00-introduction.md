Introduction
========

Cascading Style Sheets (ou simplesmente CSS) é uma linguagem de folhas de estilo utilizada para definir a apresentação de documentos escritos em uma linguagem de marcação, como HTML ou XML. Seu principal benefício é prover a separação entre o formato e o conteúdo de um documento.

Fonte: [Wikipedia](http://pt.wikipedia.org/wiki/Cascading_Style_Sheets)