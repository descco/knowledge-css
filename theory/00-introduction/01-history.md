History
========

Tim Bernes-Lee ao desenvolver o navegador Nexus quer serviu para implementar suas invenções escreveu também ainda que de forma bastante limitada, algumas funcionalidades intrínsecas que controlavam as apresentações dos documentos. Navegadores que se seguiram nos anos de 1922 e 1993 também vinham com funcionalidades de estilização padrão, tais como no modelo desenvolvido por Tim Berners-Lee no navegador Mosaic lançado em 1993 e que popularizou a web.
Na verdade apenas o controle de algumas fontes e cores era possível. As funcionalidades intrínsecas aqui referidas são aquelas que hoje em dia conhecemos como folhas de estilo padrão do navegador. Um conjunto de regras de estilo, forma uma folha de estilo que o navegador aplica aos documentos por padrão nos casos em que o autor do documento ou o usuário não tenham definido nenhuma regra de estilo.
Em setembro de 1994, surgiu a primeira proposta para implementação das CSS. Até então o próprio Tim Berners-Lee considerava que a estilização era uma questão a ser resolvida pelo próprio navegador, razão pela qual não se preocupou em publicar a sintaxe usada para criar a folha de estilo padrão do seu navegador.
No mês de dezembro de 1996 as CSS1 foram lançadas com uma recomendação oficial da W3C.

Fonte: [HTML Code](http://htmlcode.com.br/css/css-historia)